lint: 
	go run github.com/golangci/golangci-lint/cmd/golangci-lint@v1.44.2 run

fmt:
	go fmt ./...
	go mod tidy
