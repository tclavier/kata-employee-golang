package main

import (
	"github.com/google/uuid"
	"kata-employee-golang/employee"
)

func main() {
	employee.NewCompany(uuid.NewString)
}
